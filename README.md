**Prueba Práctica - Backend Developer**

Explicación breve sobre el software 

El modelo que utilice en este caso fue cliente servidor, en el cual el servidor se encarga de hacer validaciones como inserciones en base de datos con los respectivos campos.En estos mismo utilizó las entidades para crear la estructura almacenada que se hará en la base de datos, mientras que en las interfaces puede declarar para realizar mis consultas, incluyendo Queries personalizadas.El servicio lo utilizó para la transaccionalidad y así mismo para no llamar una conexión directa de controlador e interfaz.En el Controlador realizó la parte de las respuestas que se le pueden entregar el cliente, como puede ser un error de servidor, un bad_request ocasionado por el mismo cliente o por búsquedas no encontradas (NOT_FOUND) así mismo se puede hacer el mapeo de las rutas a las cuales el cliente va a consumir.

Consideraciones a utilizar  

- java 8 
- spring 4+
- Eclipse

**Test and postman**

**_add user _**

endpoint =>` https://test-valid-backend.herokuapp.com/user/new`
Method => **post**
body row json => {
    "name":String,
    "lastName":String
}

**Update process**

endpoint => ` https://test-valid-backend.herokuapp.com/user/{id}` <= _Number type id_
Method => **PUT**

**Get all user**

endpoint => ` https://test-valid-backend.herokuapp.com/user/all` <= _Number type id_
Method => **GET**


**HTML PAGE**

LINK --> [VALID TEST](https://s3.amazonaws.com/testvalid-milton-hernandez.com/index.htm)`




----
