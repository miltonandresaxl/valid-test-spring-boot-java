package com.prueba.rest.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.rest.entity.User;
import com.prueba.rest.interfaces.InUserService;

@CrossOrigin("*")
@RestController
@RequestMapping("/user")

public class UserController {
	
	@Autowired
	InUserService userService;
	
	
	@PostMapping("new")
	public ResponseEntity<?> addNewUser(@RequestBody User userBody) {
		Map<String, Object> response = new HashMap<>();
		try {	
			if(userBody.getName() == null ||  userBody.getName().isEmpty() || userBody.getLastName() == null || userBody.getLastName().isEmpty()) {
				response.put("message", "Properties name or lastname cannot be empty or null: ");
				response.put("status",  HttpStatus.BAD_REQUEST.value());
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			}
			response.put("data",userService.save(userBody));
			response.put("status",  HttpStatus.CREATED.value());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		}catch(DataAccessException ex) {
			response.put("message", "Internal server error");
			response.put("error", ex.getMessage());
			response.put("status",  HttpStatus.INTERNAL_SERVER_ERROR.value());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR	);
		}
	
	}
	
	@GetMapping("all")
	public ResponseEntity<?> getAllUser(){
		Map<String, Object> response = new HashMap<>();
		try {
			response.put("data",userService.findAll());
			response.put("status",  HttpStatus.OK.value());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		}catch(DataAccessException ex) {
			response.put("message", "Internal server error");
			response.put("error", ex.getMessage());
			response.put("status",  HttpStatus.INTERNAL_SERVER_ERROR.value());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR	);
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateProccessor(@PathVariable Long id){
		
		Map<String, Object> response = new HashMap<>();
		try {
			if( id==0 ) {
				response.put("message", "Client id: ".concat(id.toString()).concat(" cannot be 0 or null"));
				response.put("status",  HttpStatus.BAD_REQUEST.value());
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			}
			User user = userService.findById(id);
			if(user == null) {
				response.put("message", "User by id : ".concat(id.toString()).concat(" not exists!"));
				response.put("status",  HttpStatus.NOT_FOUND.value());
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
			}
			user.setName(user.getName());
			user.setLastName(user.getLastName());
			user.setProcessor(!user.getProcessor());
			
			response.put("data",userService.save(user));
			response.put("status",  HttpStatus.OK.value());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		}catch(DataAccessException ex) {
			response.put("message", "Internal server error");
			response.put("error", ex.getMessage());
			response.put("status",  HttpStatus.INTERNAL_SERVER_ERROR.value());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR	);
		}
	}
}
