package com.prueba.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prueba.rest.entity.User;
import com.prueba.rest.interfaces.InUser;
import com.prueba.rest.interfaces.InUserService;

@Service
public class UserService implements InUserService {

	@Autowired
	private InUser inUser;
	
	@Override
	@Transactional(readOnly = true)
	public List<User> findAll() {
		return (List<User>) inUser.findAll();
	}
	
	@Override
	@Transactional
	public User save(User newUser) {
		return (User) inUser.save(newUser);
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public User findById(Long id) {
		return (User) inUser.findById(id).orElse(null);
	}
	
	
	
}
