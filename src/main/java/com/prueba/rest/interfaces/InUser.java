package com.prueba.rest.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.rest.entity.User;

@Repository
public interface InUser extends JpaRepository<User, Long> {

}
