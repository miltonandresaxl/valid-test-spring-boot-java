package com.prueba.rest.interfaces;

import java.util.List;

import com.prueba.rest.entity.User;

public interface InUserService {
	
	public List<User> findAll();
	
	public User save(User newUser);
	
	public User findById(Long id);
}
